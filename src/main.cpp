#include <ros/ros.h>
#include <keyboard/Key.h>
#include <geometry_msgs/Twist.h>

#define FORWARD_SPEED 1.0
#define ROTATION_SPEED 0.5
#define CMD_DURATION 1.0

geometry_msgs::Twist vel_cmd1;
geometry_msgs::Twist vel_cmd2;
keyboard::Key key_cmd;
bool keySubFlag1 = false;
bool keySubFlag2 = false;
ros::Time t_sub_1, t_sub_2;

void keyboard_cb(const keyboard::Key::ConstPtr& msg)
{
	key_cmd = *msg;
	if((key_cmd.code == key_cmd.KEY_w) ||
       (key_cmd.code == key_cmd.KEY_s) ||
	   (key_cmd.code == key_cmd.KEY_a) ||
       (key_cmd.code == key_cmd.KEY_d))
	{
		t_sub_1 = ros::Time::now();
		keySubFlag1 = true;
	}
	if((key_cmd.code == key_cmd.KEY_i) ||
       (key_cmd.code == key_cmd.KEY_k) ||
	   (key_cmd.code == key_cmd.KEY_j) ||
       (key_cmd.code == key_cmd.KEY_l))
	{
		t_sub_2 = ros::Time::now();
		keySubFlag2 = true;
	}
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "pioneer_keyboard_teleop");
	ros::NodeHandle nh;

	ros::Publisher vel_pub1 = nh.advertise<geometry_msgs::Twist>
				("Pioneer1/RosAria/cmd_vel", 10);
	ros::Publisher vel_pub2 = nh.advertise<geometry_msgs::Twist>
				("Pioneer2/RosAria/cmd_vel", 10);
	ros::Subscriber keyboard_sub =  nh.subscribe<keyboard::Key>
				("keyboard/keydown", 10, keyboard_cb);

	vel_cmd1.linear.x = 0.0;
	vel_cmd1.linear.y = 0.0;
	vel_cmd1.linear.z = 0.0;
	vel_cmd1.angular.x = 0.0;
	vel_cmd1.angular.y = 0.0;
	vel_cmd1.angular.z = 0.0;
	vel_cmd2.linear.x = 0.0;
	vel_cmd2.linear.y = 0.0;
	vel_cmd2.linear.z = 0.0;
	vel_cmd2.angular.x = 0.0;
	vel_cmd2.angular.y = 0.0;
	vel_cmd2.angular.z = 0.0;

	ros::Rate rate(50.0);
	while(ros::ok())
	{
		ros::spinOnce();
		if(keySubFlag1)
		{
			vel_cmd1.linear.x = 0.0;
			vel_cmd1.linear.y = 0.0;
			vel_cmd1.linear.z = 0.0;
			vel_cmd1.angular.x = 0.0;
			vel_cmd1.angular.y = 0.0;
			vel_cmd1.angular.z = 0.0;
			if(key_cmd.code == key_cmd.KEY_w)
			{
				vel_cmd1.linear.x = FORWARD_SPEED;
			}
			else if(key_cmd.code == key_cmd.KEY_s)
			{
				vel_cmd1.linear.x = -FORWARD_SPEED;
			}
			else if(key_cmd.code == key_cmd.KEY_a)
			{
				vel_cmd1.angular.z = ROTATION_SPEED;
			}
			else if(key_cmd.code == key_cmd.KEY_d)
			{
				vel_cmd1.angular.z = -ROTATION_SPEED;
			}
			keySubFlag1 = false;
		}
		if(keySubFlag2)
		{
			vel_cmd2.linear.x = 0.0;
			vel_cmd2.linear.y = 0.0;
			vel_cmd2.linear.z = 0.0;
			vel_cmd2.angular.x = 0.0;
			vel_cmd2.angular.y = 0.0;
			vel_cmd2.angular.z = 0.0;
			if(key_cmd.code == key_cmd.KEY_i)
			{
				vel_cmd2.linear.x = FORWARD_SPEED;
			}
			else if(key_cmd.code == key_cmd.KEY_k)
			{
				vel_cmd2.linear.x = -FORWARD_SPEED;
			}
			else if(key_cmd.code == key_cmd.KEY_j)
			{
				vel_cmd2.angular.z = ROTATION_SPEED;
			}
			else if(key_cmd.code == key_cmd.KEY_l)
			{
				vel_cmd2.angular.z = -ROTATION_SPEED;
			}
			keySubFlag2 = false;
		}
		if((ros::Time::now() - t_sub_1).toSec()>=CMD_DURATION)
		{
			vel_cmd1.linear.x = 0.0;
			vel_cmd1.linear.y = 0.0;
			vel_cmd1.linear.z = 0.0;
			vel_cmd1.angular.x = 0.0;
			vel_cmd1.angular.y = 0.0;
			vel_cmd1.angular.z = 0.0;
		}
		if((ros::Time::now() - t_sub_2).toSec()>=CMD_DURATION)
		{
			vel_cmd2.linear.x = 0.0;
			vel_cmd2.linear.y = 0.0;
			vel_cmd2.linear.z = 0.0;
			vel_cmd2.angular.x = 0.0;
			vel_cmd2.angular.y = 0.0;
			vel_cmd2.angular.z = 0.0;
		}
		vel_pub1.publish(vel_cmd1);
		vel_pub2.publish(vel_cmd2);
		rate.sleep();	
	}
	return 0;
}
